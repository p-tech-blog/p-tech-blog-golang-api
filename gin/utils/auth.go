package utils

import (
	"fmt"
	"log"
	"os"

	"p-tech-blog-golang-api/api/models"

	"github.com/dgrijalva/jwt-go"
)

type User struct {
	Account  string `json:account`
	Password string `json:password`
}

func GenerateToken(user models.UserLoginInput) (string, error) {
	var err error
	secret := os.Getenv("SECRET")

	// a jwt = header.payload.secret, the secret is the part of third jwt part

	// 第一個參數: 要使用的密碼演算法
	// 第二個參數: 要求的參數
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"acc": user.Account,
		"iss": "test",
	})

	tokenstring, err := token.SignedString([]byte(secret))

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("token", token)

	return tokenstring, nil
}
