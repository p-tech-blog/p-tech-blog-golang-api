package controllers

import (
	"net/http"
	"p-tech-blog-golang-api/gin/driver"
	"p-tech-blog-golang-api/gin/models"

	"github.com/gin-gonic/gin"
)

func UpdateUserPassword(c *gin.Context) {
	var input models.UpdateUserPasswordInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var user models.User
	if err := driver.DB.Where("id = ?", c.Param("id")).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	driver.DB.Model(&user).Update(input)

	c.JSON(http.StatusOK, gin.H{"code": 0, "content": ""})
}
