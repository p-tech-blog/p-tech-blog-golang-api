package controllers

import (
	"net/http"
	"p-tech-blog-golang-api/gin/driver"
	"p-tech-blog-golang-api/gin/models"

	"github.com/gin-gonic/gin"
)

func DeleteUser(c *gin.Context) {
	var user models.User
	if err := driver.DB.Where("id = ?", c.Param("id")).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	driver.DB.Delete(&user)

	c.JSON(http.StatusOK, gin.H{"code": 0, "content": ""})
}
