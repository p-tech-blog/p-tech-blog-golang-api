package controllers

import (
	"net/http"
	"p-tech-blog-golang-api/gin/driver"
	"p-tech-blog-golang-api/gin/models"

	"github.com/gin-gonic/gin"
)

func DeleteSubCategory(c *gin.Context) {
	var subCategory models.SubCategory
	if err := driver.DB.Where("id = ?", c.Param("id")).First(&subCategory).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	driver.DB.Delete(&subCategory)

	c.JSON(http.StatusOK, gin.H{"code": 0, "content": ""})
}
