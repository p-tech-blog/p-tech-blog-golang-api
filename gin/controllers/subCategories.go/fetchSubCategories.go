package controllers

import (
	"net/http"
	"p-tech-blog-golang-api/gin/driver"
	"p-tech-blog-golang-api/gin/models"

	"github.com/gin-gonic/gin"
)

func FetchSubCategories(c *gin.Context) {

	var subCategories []models.SubCategory
	driver.DB.Find(&subCategories)

	c.JSON(http.StatusOK, gin.H{"code": 0, "content": subCategories})
}
