package controllers

import (
	"fmt"
	"net/http"
	"p-tech-blog-golang-api/gin/driver"
	"p-tech-blog-golang-api/gin/models"

	"github.com/gin-gonic/gin"
)

func UpdateSubCategory(c *gin.Context) {

	// Validate input
	var input models.UpdateSubCategoryInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var subCategory models.SubCategory
	if err := driver.DB.Where("id = ?", c.Param("id")).First(&subCategory).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	fmt.Println("!!!", input.SubCategory)
	fmt.Println("!!!", c.Param("id"))
	fmt.Println("!!!", subCategory)

	driver.DB.Model(&subCategory).Update(input)

	c.JSON(http.StatusOK, gin.H{"code": 0, "content": ""})
}
