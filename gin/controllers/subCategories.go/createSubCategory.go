package controllers

import (
	"net/http"
	"p-tech-blog-golang-api/gin/driver"
	"p-tech-blog-golang-api/gin/models"

	"github.com/gin-gonic/gin"
)

func CreateSubCategory(c *gin.Context) {

	// Validate input
	var input models.SubCategory
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	subCategory := models.SubCategory{
		SubCategory: input.SubCategory,
		CategoryID:  input.CategoryID,
	}
	driver.DB.Create(&subCategory)

	c.JSON(http.StatusOK, gin.H{"code": 0, "content": ""})
}
