package controllers

import (
	"net/http"
	"p-tech-blog-golang-api/gin/driver"
	"p-tech-blog-golang-api/gin/models"

	"github.com/gin-gonic/gin"
)


func CreateCategory(c *gin.Context) {

	// Validate input
	var input models.Category
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	category := models.Category{Category: input.Category}
	driver.DB.Create(&category)

	c.JSON(http.StatusOK, gin.H{"code": 0, "content": ""})
}
