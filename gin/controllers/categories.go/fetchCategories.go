package controllers

import (
	"net/http"
	"p-tech-blog-golang-api/gin/driver"
	"p-tech-blog-golang-api/gin/models"

	"github.com/gin-gonic/gin"
)


func FetchCategories(c *gin.Context) {

	var categories []models.Category
	driver.DB.Find(&categories)

	c.JSON(http.StatusOK, gin.H{"content": categories})
}
