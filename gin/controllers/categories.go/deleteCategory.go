package controllers

import (
	"net/http"
	"p-tech-blog-golang-api/gin/driver"
	"p-tech-blog-golang-api/gin/models"

	"github.com/gin-gonic/gin"
)

func DeleteCategory(c *gin.Context) {
	var category models.Category
	if err := driver.DB.Where("id = ?", c.Param("id")).First(&category).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	driver.DB.Delete(&category)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
