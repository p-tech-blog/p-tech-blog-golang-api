package controllers

import (
	"log"
	"net/http"
	"p-tech-blog-golang-api/gin/models"
	"p-tech-blog-golang-api/gin/utils"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

func Login(c *gin.Context) {

	var input models.UserLoginInput
	hashedPassword := input.Password
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "帳號或密碼錯誤"})
		return
	}

	// 產生一個token
	token, err := utils.GenerateToken(input)
	if err != nil {
		log.Fatal(err)
	}

	type ResBody struct {
		Token string `json:token`
	}
	resBody := ResBody{token}
	c.JSON(http.StatusOk, gin.H{"code": 0, "content": resBody})
}
