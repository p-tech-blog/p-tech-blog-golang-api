package controllers

import (
	"net/http"
	"p-tech-blog-golang-api/gin/driver"
	"p-tech-blog-golang-api/gin/models"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

func CreateUser(c *gin.Context) {
	var input models.User
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	hashPwd, err := bcrypt.GenerateFromPassword([]byte(input.Password), 10)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	input.Password = string(hashPwd)

	user := models.User{
		Account:  input.Account,
		Password: input.Password,
		Status:   input.Status,
	}

	driver.DB.Create(&user)

	c.JSON(http.StatusOK, gin.H{"code": 0, "content": ""})

}
