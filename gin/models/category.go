package models

import (
	"github.com/jinzhu/gorm"
)

type Category struct {
	gorm.Model
	Category string `json:"category"`
}

type UpdateCategoryInput struct {
	Category string `json:"category"`
}
