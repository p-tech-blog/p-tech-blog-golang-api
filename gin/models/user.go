package models

import (
	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	Account  string `json:"account"`
	Password string `json:"password"`
	Status   int    `json:"status"`
}

type UserLoginInput struct {
	Account  string `json:"account"`
	Password string `json:"password"`
}

type UpdateUserPasswordInput struct {
	Password string `json:"password"`
}

type UpdateUserStatusInput struct {
	Status int `json:"status"`
}
