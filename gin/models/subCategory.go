package models

import (
	"github.com/jinzhu/gorm"
)

type SubCategory struct {
	gorm.Model
	CategoryID  uint   `json:"categoryID"`
	SubCategory string `json:"subCategory"`
}

type UpdateSubCategoryInput struct {
	CategoryID  uint   `json:"categoryID"`
	SubCategory string `json:"subCategory"`
}
