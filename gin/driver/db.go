package driver

import (
	"p-tech-blog-golang-api/gin/models"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var DB *gorm.DB

func ConnectDatabase() {

	db, err := gorm.Open("mysql", "root:12345678@tcp(localhost:3306)/dev?charset=utf8&parseTime=True&loc=Local")
	// defer db.Close()

	if err != nil {
		panic("Failed to connect to db!")
	}

	// db.AutoMigrate(&models.User{})
	db.AutoMigrate(&models.Category{})
	db.AutoMigrate(&models.SubCategory{})

	DB = db
}
