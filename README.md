# p-tech-blog-golang-api

golang-API

## TODO

#### priority

- 統一回傳格式
- article table 未正規化完成，resources 要來源分離到另一張 table
- log
- 根據條件查詢 article api
- 拿現有分類含子分類的 api
- 加入權限 middleware
- 分頁
- defer rows.Close()

#### second priority

- 驗證參數型別是否正確
- 驗證是否可以有 query string 和參數等規則
- timeout
- 壓力測試
- go routine

### last

- 移除未使用的依賴 mux 之類的
