package main1

import (
	"github.com/gin-gonic/gin"

	"p-tech-blog-golang-api/gin/driver"
	"p-tech-blog-golang-api/gin/middleware"

	// hello "p-tech-blog-golang-api/gin/controllers"

	categoriesCtrls "p-tech-blog-golang-api/gin/controllers/categories.go"
	subCategoriesCtrls "p-tech-blog-golang-api/gin/controllers/subCategories.go"

	// testCtrls "p-tech-blog-golang-api/gin/controllers/test.go"
	"p-tech-blog-golang-api/gin/controllers"
)

func main() {
	r := gin.Default()

	// CORS
	r.Use(middleware.CORSMiddleware())

	driver.ConnectDatabase()

	// r.GET("/test", subCategoriesCtrls.Test)

	// 使用者
	// r.GET("/users", usersCtrls.FetchCategories)
	// r.GET("/users/:id", usersCtrls.FetchCategory)
	r.POST("/users", controllers.CreateUser)
	r.POST("/login", controllers.Login)
	r.PUT("/users/:id/password", controllers.UpdateUserPassword)
	r.PUT("/users/:id/status", controllers.UpdateUserStatus)
	r.DELETE("/users/:id", controllers.DeleteUser)

	// 分類
	r.GET("/categories", categoriesCtrls.FetchCategories)
	r.GET("/categories/:id", categoriesCtrls.FetchCategory)
	r.POST("/categories", categoriesCtrls.CreateCategory)
	r.PUT("/categories/:id", categoriesCtrls.UpdateCategory)
	r.DELETE("/categories/:id", categoriesCtrls.DeleteCategory)

	// 子分類
	r.GET("/subCategories", subCategoriesCtrls.FetchSubCategories)
	r.GET("/subCategories/:id", subCategoriesCtrls.FetchSubCategory)
	r.POST("/subCategories", subCategoriesCtrls.CreateSubCategory)
	r.PUT("/subCategories/:id", subCategoriesCtrls.UpdateSubCategory)
	r.DELETE("/subCategories/:id", subCategoriesCtrls.DeleteSubCategory)

	r.Run()
}
